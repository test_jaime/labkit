#!/usr/bin/env bash

set -eo pipefail -x

repo=$1
cloneDir=$(mktemp -d)
CI_COMMIT_SHA=${CI_COMMIT_SHA:-master}

git clone "$repo" "$cloneDir"
cd "$cloneDir"

if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
  go get gitlab.com/gitlab-org/labkit@"$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA"
else
  go get gitlab.com/gitlab-org/labkit@"$CI_COMMIT_SHA"
fi

# Ensure go.mod and go.sum are up to date in the cloned repo, otherwise build may fail.
go mod tidy

make

rm -rf "$cloneDir"
