package errortracking

import (
	"reflect"

	"github.com/getsentry/sentry-go"
)

type sentryTracker struct {
	hub *sentry.Hub
}

func newSentryTracker(hub *sentry.Hub) *sentryTracker {
	return &sentryTracker{
		hub: hub,
	}
}

func (s *sentryTracker) Capture(err error, opts ...CaptureOption) {
	_, event := applyCaptureOptions(opts)

	event.Exception = []sentry.Exception{
		{
			Type:       reflect.TypeOf(err).String(),
			Value:      err.Error(),
			Stacktrace: sentry.ExtractStacktrace(err),
		},
	}
	s.hub.CaptureEvent(event)
}
